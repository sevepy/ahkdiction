; This Source Code Form is subject to the terms of the Mozilla Public
; License, v. 2.0. If a copy of the MPL was not distributed with this
; file, You can obtain one at https://mozilla.org/MPL/2.0/.

#SingleInstance Force
#include TextRender.ahk
DetectHiddenWindows,On

SetWorkingDir %A_ScriptDir% 
SetBatchLines -1

EnvGet, vHomeDrive, HOMEDRIVE
EnvGet, vHomePath, HOMEPATH
HOME := vHomeDrive . vHOMEPATH 
FONT_SIZE := GetConfig("UI","FontSize","12")
GUI_FONT := GetConfig("UI","GuiFont","MS Sans Serif")
LANG := GetConfig("Lang","Lang","en_GB")    
BUFFERFILE := GetConfig("OUTPUT","Buffer",".scratch_buffer")

; Pandoc config
OutputFormat := GetConfig("Pandoc","OutputFormat","docx")    
OutputDir := GetConfig("Pandoc","OutputDir","Documents")    
DataDir := GetConfig("Pandoc","DataDir", ".pandoc") 
Options := GetConfig("Pandoc","Options", "") 
DataDir := HOME . "\" . DataDir

EnvSet, PATH, %A_ScriptDir%
Gui Font, s%FONT_SIZE%, GUI_FONT
Gui Add, Button, x24 y9 w120 h39, Select File
Gui Add, Button, x164 y9 w120 h39, Use Clipboard
Gui Add, Button, x300 y9 w120 h39, Edit Config
Gui Add, Button, x440 y9 w120 h39, Paste to Clipboard
Gui Add, Button, x24 y59 w120 h39, Docx
Gui Add, Button, x164 y59 w120 h39, Pandoc Logs
Gui Add, Button, x300 y59 w120 h39, VIM
Gui Add, Button, x440 y59 w120 h39, Restart
Gui,Add, Text,x24 y109 w567 h39 vDictionTmp, Selected file
Gui Add, Edit, x24 y129 w567 h355 +VScroll vText cBlack , 
Gui Add, Text, x24 y484 w120 h23 +0x200, Press Esc to quit.
Gui Add, Text, x353 y484 w257 h23 +0x200, S Tessarin (c) 2023 v. 0.2.2
Gui Add, Link, x154 y486 w160 h23, <a href="https://www.buymeacoffee.com/seve">Support this project.</a>
Gui Show, w620 h520, WinDiction
StringUpper PandocButtonLabel, OutputFormat
GuiControl,, Docx, %PandocButtonLabel%


return  ; End of auto-execute section. The script is idle until the user does something.

ButtonEditConfig:
Run, %A_ScriptDir%\config.ini
return

ButtonRestart:
Reload
return

ButtonVIM:
cpvimrc:="
(

let g:windiction_dir='" A_ScriptDir "'
let g:windiction_lang='" LANG "'

)"
MsgBox,4096,Copy/Paste to the main .vimrc file, %cpvimrc%
SetEnv Clipboard, %cpvimrc%
return

ButtonUseClipboard:
GuiControl,,Text,
Tfile := A_ScriptDir "\\.diction_tmp"
FileDelete %Tfile%  ; In case previous run was terminated prematurely.
FileAppend,%Clipboard%,%Tfile%,CP65001

GuiControl,,DictionTmp, Windows Clipboard
run_diction(Tfile,1)
return


ButtonPandocLogs:
;TODO parse json
Plogs := A_ScriptDir "\\pandoc.log"
FileRead, logs, %Plogs%
MsgBox %logs%
return


ButtonPastetoClipboard:
Tfile := A_ScriptDir "\\.diction_tmp"
FileRead, raw, %Tfile%
if raw=""
{
raw="Nothing to Paste"
tr := TextRender(raw, "time:2000 w:620px h:220px r:2% c:darkblue ", "j:left n:1")
}
else 
{
MsgBox,4096,Saved to clipboard, %raw%
}
SetEnv Clipboard, %raw%
return

ButtonDocx:
Global CurrentFile
if (CurrentFile != "")
{

SplitPath, CurrentFile,name,dir,ext,,
Ofile:= HOME "\" . OutputDir "\" . name . "." . OutputFormat


    if (ext="md" OR ext="txt")
    {
fe := "markdown"
    }
    else if (ext="docx")
    {
fe := "docx"
    }
    else if (ext="odt")
    {
fe:="odt"
    }
    else if (ext = "wiki")
    {
fe := "mediawiki"
    }
    else if (ext = "ipynb")
    {
fe := "ipynb"
    }
    else
    {

tr := TextRender("File type not supported.", "time:2000 w:620px h:220px r:2% c:darkblue ", "j:left n:1")
    return
    }
; fix filenames for pandoc command line 
if (Options != "")
{

dictCMD := a_scriptdir . "\\pandoc.exe --log=pandoc.log --data-dir=" . DataDir . " " . Options . " -f " . fe . " -t " . OutputFormat . " " . """" . CurrentFile . """" . " -o " . """" . Ofile . """"  
}
else{
dictCMD := a_scriptdir . "\\pandoc.exe --log=pandoc.log --data-dir=" . DataDir . " -f " . fe . " -t " . OutputFormat . " "  . """" . CurrentFile . """" . " -o " . """" . Ofile . """"
}


if FileExist(Ofile){
MsgBox, 4, , Output file already exists, Would you like to continue?
IfMsgBox, No
    Return  ; User pressed the "No" button.
}

cmdout := RunWaitOne(dictCMD)
if FileExist(Ofile)
{
GuiControl,,Text, %dictCMD% `r`n`r`n Output File:%Ofile%
}
else
{
GuiControl,,Text, %dictCMD% `r`n Error: `r`n`r`n %Ofile% not generated.`r`n %cmdout%:
}
}
else 
{
tr := TextRender("Select a supported file: .md .txt .wiki .docx .odt .ipyn  .", "time:2000 w:620px h:220px r:2% c:darkblue ", "j:left n:1")
}
return


ButtonSelectFile:
Global CurrentFile
GuiControl,,Text,
FileSelectFile, SelectedFile, 3, , Open a file (*.txt; *.md; *.wiki; *.docx; *.odt)
if (SelectedFile = "")
	Return
CurrentFile := SelectedFile
GuiControl,,DictionTmp, %CurrentFile%
run_diction(SelectedFile,0)

Return


GetConfig(section, key, default) {
    IniRead value, %A_ScriptDir%\config.ini, %section%, %key%, %default%
    return value
}

run_diction(Runfile,flag) {
    global Lang
    global BUFFERFILE
    EnvGet, UserProfile, UserProfile
    OutputFile := UserProfile . "\\" . BUFFERFILE
    FileDelete %OutputFile% 
    Tfile := A_ScriptDir "\\.diction_tmp"
	;global Tfile
	; localbash.exe a copy of  msys2 bash.exe from /usr/bin/bash.exe
	; ---------------------> Mingw file path
	;Tfile :=  RegExReplace( Tfile , "\\", "/")
	;Tfile :=  RegExReplace( Tfile , "^([^:]+):", "/$1")
	;better but still not happy with white spaces
    if (flag = 0) {
        SplitPath, Runfile,name,dir,ext,,

            if (ext="md"){

fe := "markdown"

            }
            else if (ext="docx")
            {
fe := "docx"
            }    
            else if (ext="odt")
            {
fe:="odt"
            }
            else if (ext = "wiki")
            {
fe := "mediawiki"
            }
            else 
            {

flag := 2
            }
    }

    ; -q quiet option toggle line-col-endline-endcol at the start of a sentence
	; dictCMD := ".\bash.exe -lc 'diction.exe -q -s -L en_GB " . Tfile . "'"
	; compiled with 32 bit mingw requires libtre-5.dll in the same dir
    if (flag = 0){
        dictCMD := "pandoc.exe -f " . fe . " -t plain " . """" . Runfile . """" . " -o " . Tfile . " && diction.exe -q -s -L " . Lang . " " . Tfile   
    }
    else if (flag =1 ) {

        dictCMD := "diction.exe -q -s -L " . Lang . " " . Tfile   
    }
    else if (flag = 2 ){
    FileRead, TextContent, %Runfile%
    if not ErrorLevel  ; Successfully loaded.
    {
    SetEnv Clipboard, %TextContent%
            dictCMD := "diction.exe -q -s -L " . Lang . " """ . Runfile . """"  
            ;directly pass a txt file to diction (weak)
FileDelete %Tfile%  ; In case previous run was terminated prematurely.
FileAppend,%Clipboard%,%Tfile%,CP65001
    }
    else
        {
        MsgBox %Runfile% unreadable. Error %ErrorLevel% 
        }
    }
    CMD=%dictCMD%
        Shell := ComObjCreate("WScript.Shell")
        Output :=""
        Shell.Exec(ComSpec " /Q /C ")
        objExec := Shell.Exec(ComSpec " /Q /C " CMD)
        while,!objExec.StdOut.AtEndOfStream
        {
            GuiControlGet,Text 
                strStdOut:= objExec.StdOut.readline() 
                Output := Output . RegExReplace(strStdOut, "'r'n", "") . "`n" 
                GuiControl,,Text,%Text% `r`n%strStdOut%
        }

    FileAppend,%Output%,%OutputFile%,CP65001
}

RunWaitOne(command) {
    ; WshShell object: http://msdn.microsoft.com/en-us/library/aew9yb99
    shell := ComObjCreate("WScript.Shell")
    ; Execute a single command via cmd.exe
    exec := shell.Exec(ComSpec " /C " command)
    ; Read and return the command's output
    return exec.StdOut.ReadAll()
}
esc::ExitApp
