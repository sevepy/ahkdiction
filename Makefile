VLANG=~/workplace/v/v.exe

diction_linter.exe:diction_linter.v
	$(VLANG) diction_linter.v

win32: ahkdiction.ahk 
	- start  windiction_Compile.ahk
	#cp diction_linter.exe ./windiction

clean:
	find . -name \.diction_tmp -type f -exec rm -f {} \;

test:
	cp ./windiction/windiction.exe $${HOME}/AppData/Local/Programs/WinDiction/
