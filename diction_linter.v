import os

fn main() {
    
    lang := os.args[1]
    quoted_filename := os.args[2]
    filename := quoted_filename[1..quoted_filename.len-1]
    // diction can only process files local to the executable
    // problem with filenames with spaces
    //println("$lang   $filename")
    mut r := os.execute("diction.exe -s -L $lang \"$filename\"")
    mut f := os.create('.error_list') or {
                println(err)
                return
        }
    rows := r.output.split('\r\n') 
    for i,row in rows {
        if row != '' && i<rows.len-2
            {
        mut pos_suggestion := row.split("]]")
        mut pos := pos_suggestion[0]
        mut suggestion := pos_suggestion[1]

        mut rowlin := pos.split(" ")
        mut line := rowlin[1]
        mut col := rowlin[2]
        //println(col[0])
        if col[0] == 69
            {col = rowlin[3]}

        mut getcol := (col[1..]).int() 

        f.write_string(filename+":"+line[1..]+":$getcol:$suggestion\r\n")?
        }
    }
}
