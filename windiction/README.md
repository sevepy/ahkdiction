#  Windiction en_GB

Gnu-diction for Windows with an easy to use graphical interface. It identifies wordy and commonly misused phrases, not a grammar checker. It supports the original en_GB dictionary.


## Installation

Download the Windows self-extracting installer and double-click on the installer itself.
Select your destination folder ( desktop - recommended ), when completed, enter the folder and then
double click on windiction_en_GB.exe.

- Select a text file:

Click the Select File button on the top left side corner and select the text file you want to 
check for diction.

- Clipboard:

Alternative you can copy any text selected to the clipboard (CTRL-C) and 
then click 'Use Clipboard' to check the text against the default dictionary. 

## Remove the app

Delete the folder app.

Copyright (C) 2020 S. Tessarin
twitter : @Seve_py

Read the related tech-note at https://www.buymeacoffee.com/seve